def dict_to_praat(grid_dict, export_path):
    """Convert a TGRE generated TextGrid dictionary to Praat TextGrid text format"""

    # Parse dictonary and convert to string
    tab = '    ' # or '\t' ?
    s = 'File type = "ooTextFile"\n'
    s += 'Object class = "TextGrid"\n\n'
    s += f'xmin = {grid_dict["xmin"]}\n'
    s += f'xmin = {grid_dict["xmax"]}\n'
    s += 'tiers? <exists>\n'
    s += f'size = {len(grid_dict["tiers"])}\n'
    s += f'item []:\n'
    for tier_idx, tier in enumerate(grid_dict['tiers']):
        s += f'{tab}item [{tier_idx+1}]:\n'
        s += f'{tab*2}class = "{tier["class"]}"\n'
        s += f'{tab*2}name = "{tier["name"]}"\n'
        s += f'{tab*2}xmin = {tier["xmin"]}\n'
        s += f'{tab*2}xmax = {tier["xmax"]}\n'
        if (tier['class'] == 'IntervalTier'):
            s += f'{tab*2}intervals: size = {len(tier["intervals"])}\n'
            for int_idx, interval in enumerate(tier['intervals']):
                s += f'{tab*2}intervals [{int_idx+1}]:\n'
                s += f'{tab*3}xmin = {interval["xmin"]}\n'
                s += f'{tab*3}xmax = {interval["xmax"]}\n'
                s += f'{tab*3}text = "{interval["text"]}"\n'
        if (tier['class'] == 'TextTier'):
            s += f'{tab*2}points: size = {len(tier["points"])}\n'
            for point_idx, point in enumerate(tier['points']):
                s += f'{tab*2}points [{point_idx+1}]:\n'
                s += f'{tab*3}number = {point["number"]}\n'
                s += f'{tab*3}mark = "{point["mark"]}"\n'

    with open(export_path, 'w') as expf:
        expf.write(s)
