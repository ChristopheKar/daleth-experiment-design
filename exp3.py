import os
import numpy as np
from scipy.io import wavfile
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import bioread
import tgre
from praat import dict_to_praat
import pandas as pd

data_dir = 'data/groups/Group3'

BPM = 120
CYCLE_THRESH = 6
cols = ['Subject', 'Group', 'Condition', 'Task', 'TrainNb', 'BeatNb', 'BeatInstant', 'TapInstant']
df = pd.DataFrame(columns=cols)


def get_task_info(filepath):
    """ Get group, subject, condition, and task information from filepath"""
    group, subject, condition = filepath.split('/')[2:5]
    group = int(group.split('Group')[-1])
    subject = int(subject.split('S')[-1])
    # get condition
    if (condition == 'Aperiodic'):
        condition = 'AP'
    elif (condition == 'PeriodicAlong'):
        condition = 'PA'
    else:
        raise ValueError('Undefined value for condition {}'.format(condition))

    task = 'FT' # finger tapping

    return group, subject, condition, task


def read_wavfile(filepath):
    """Read wavfile and extract rate, response channel, stimulus channel"""
    # read data .wav file
    sampling_rate, data = wavfile.read(filepath)
    # process data channels into stimulus and response
    stimulus = data[:, 0]
    response = data[:, 1]

    return sampling_rate, stimulus, response


def detect_peaks(sig, fs, condition, mode='stimulus'):
    """Detect peaks in stimulus or response signals under PA or AP"""
    # set processing parameters based on condition
    if (condition == 'AP'):
        init_lag = int(fs*2)
        height_thresh = 0.2
        dist_thresh = fs*0.2
        neighbor_thresh = None
        prominence = None
        width = (None, 9)
        if (mode == 'response'):
            width = None
            neighbor_thresh = None
            prominence = 0.1
            dist_thresh = fs*0.25
    elif (condition == 'PA'):
        init_lag = int(fs*3)
        height_thresh = 0.2
        dist_thresh = fs*0.3
        width = None
        neighbor_thresh = None
        prominence = None
    else:
        raise ValueError('Undefined value for condition {}'.format(condition))
    # normalize and cut signal
    sig_norm = sig[init_lag:]/sig.max()
    # find peak indexes and heights
    sig_peaks_idx, sig_peaks_info = find_peaks(sig_norm,
                                               height=height_thresh,
                                               distance=dist_thresh,
                                               width=width)
    sig_peaks_idx += init_lag # add lag back to signal
    sig_peaks_y = sig_peaks_info['peak_heights']*sig.max()

    return sig_peaks_idx, sig_peaks_y

def detect_cycles(peaks_idx, fs, bpm, condition):
    # set processing parameters based on condition
    if (condition == 'AP'):
        safety_factor = 0.5
        lower_lim = int(peaks_idx[0] - (fs*safety_factor))
        upper_lim = int(peaks_idx[-1] + (fs*safety_factor))
        return [peaks_idx], [(lower_lim, upper_lim)], []
    elif (condition == 'PA'):
        # get theoretical beat distance
        beat_dist = (60/bpm)*fs
        dist_thresh = 0.05
        # get distance between peaks
        peaks_idx_diff = np.diff(peaks_idx,
                                 append=[peaks_idx[-1] + beat_dist*100])
        # generate mask for which beats are grouped together
        cycle_masks = np.abs((peaks_idx_diff/beat_dist) - 1) < 0.05
        # generate intervals
        safety_factor = 0.3
        grp = []
        discard = []
        cycles = []
        bounds = []
        for idx, mask in zip(peaks_idx, cycle_masks):
            if (mask):
                grp.append(idx)
            else:
                grp.append(idx)
                if len(grp) >= CYCLE_THRESH:
                    cycles.append(grp)
                    bounds.append((
                        int(grp[0] - (fs*safety_factor)),
                        int(grp[-1] + (fs*safety_factor))
                    ))
                else:
                    # noise groups: not enough peaks
                    for g in grp:
                        discard.append(g)
                grp = []
        return cycles, bounds, discard
    else:
        raise ValueError('Undefined value for condition {}'.format(condition))


def get_wavfiles(data_dir):
    """Traverse data directory and return list of wav files"""
    wavfiles = []
    for root, dirs, files in os.walk(data_dir):
        for file in files:
            if file.endswith('.wav'):
                filepath = os.path.join(root, file)
                wavfiles.append(filepath)
    return wavfiles

data = []
wavfiles = get_wavfiles(data_dir)
for filepath in wavfiles:
    # get group, subject, and condition information
    group, subject, condition, task = get_task_info(filepath)

    # read data .wav file
    fs, stim, resp = read_wavfile(filepath)
    # detect stimulus peaks and cycles
    stim_peaks_idx, stim_peaks_y = detect_peaks(stim, fs, condition)
    stim_cycles, cycle_bounds, discard = detect_cycles(stim_peaks_idx,
                                                       fs,
                                                       BPM,
                                                       condition)

    # discard stimulus noise cycles by setting peak height to nan
    discard_idxs = np.where(sum([stim_peaks_idx == d for d in discard]))
    stim_peaks_y[discard_idxs] = np.nan
    # detect response peaks
    resp_peaks_idx, resp_peaks_y = detect_peaks(resp,
                                                fs,
                                                condition,
                                                'response')

    # discard response peaks outside of stimulus cycles
    resp_cycles = [list(resp_peaks_idx[(resp_peaks_idx > c[0]) & (resp_peaks_idx < c[1])
    ].astype('int')) for c in cycle_bounds]
    discard_idxs = np.invert(
        (sum([(resp_peaks_idx > c[0]) & (resp_peaks_idx < c[1]) \
                for c in cycle_bounds])).astype('bool'))
    resp_peaks_y[discard_idxs] = np.nan

    # plot signals
    fig, ax = plt.subplots(2, 1, figsize=(25, 10))
    ax[0].plot(stim)
    ax[0].plot(stim_peaks_idx, stim_peaks_y, 'o')
    for cycle in cycle_bounds:
        ax[0].vlines(cycle[0], stim.min(), stim.max(), linestyles='--')
        ax[0].vlines(cycle[1], stim.min(), stim.max(), linestyles='--')
        ax[1].vlines(cycle[0], stim.min(), stim.max(), linestyles='--')
        ax[1].vlines(cycle[1], stim.min(), stim.max(), linestyles='--')
    ax[1].plot(resp)
    ax[1].plot(resp_peaks_idx, resp_peaks_y, 'o')
    # fig.show()


    # for the APeriodic task condition, make decisions on how to pair-up
    # beats and taps through a bounded-nearest-neighbor strategy
    if (condition == 'AP'):
        tmp_resp_cycles = []
        for cnum, (scycle, rcycle) in enumerate(zip(stim_cycles, resp_cycles)):
            tmp_rcycle = []
            for beat_idx in scycle:
                # choose the nearest tap from taps preceeding the beat
                # by 2% of fs and succeeding it by 50% of fs.
                idxs = np.asarray(
                    (rcycle > (beat_idx - fs*0.02))
                    & (rcycle < (beat_idx + fs*0.5))
                    & (rcycle != np.nan)
                ).nonzero()[0]
                if (idxs.shape[0] > 0):
                    rcycle = np.asarray(rcycle)
                    tap_idxs = rcycle[idxs]
                    nearest_tap_idx = idxs[(np.abs(tap_idxs - beat_idx)).argmin()]
                    tmp_rcycle.append(rcycle[nearest_tap_idx])
                    rcycle[nearest_tap_idx] = 0
                else:
                    tmp_rcycle.append(np.inf)
            tmp_resp_cycles.append(tmp_rcycle)

        resp_cycles = tmp_resp_cycles.copy()


    # construct dataframe list
    for cnum, (scycle, rcycle) in enumerate(zip(stim_cycles, resp_cycles)):
        # print(cnum + 1)
        for bnum, (beat, tap) in enumerate(zip(scycle, rcycle)):
            # print(bnum+1, beat, tap)
            data.append({
                'group': group,
                'subject': subject,
                'condition': condition,
                'task': task,
                'cycle_num': cnum + 1,
                'beat_num': bnum + 1,
                'beat_instant': beat/fs,
                'tap_instant': tap/fs
            })

df = pd.DataFrame(data=data)
df['reaction_time'] = df['tap_instant'] - df['beat_instant']
