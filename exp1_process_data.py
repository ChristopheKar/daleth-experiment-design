import numpy as np
from scipy.io import wavfile
from scipy.signal import find_peaks
import matplotlib.pyplot as plt

# Define periodic data settings
periodic = {
    'stim_path': 'data/exp1/PeriodicAlong.wav',
    'resp_path': 'data/exp1/periodic_rec.npy',
    'stim_channel': 0,
    'resp_channel': 0,
    'fs': 20000,
    'dist_factor': 1,
    'xlim': (int(0.32*1e6), int(2.5*1e6)),
    'height_thresh': 0.2,
}

aperiodic = {
    'stim_path': 'data/exp1/Aperiodic.wav',
    'resp_path': 'data/exp1/aperiodic_rec.npy',
    'stim_channel': None,
    'resp_channel': 0,
    'fs': 20000,
    'dist_factor': 0.5,
    'xlim': (int(0.30*1e6), int(1.25*1e6)),
    'height_thresh': 0.28,
}


# Load stimulus .wav files
periodic['rate'], periodic['stim'] = wavfile.read(periodic['stim_path'])
aperiodic['rate'], aperiodic['stim'] = wavfile.read(aperiodic['stim_path'])

# Load response .npy files
periodic['resp'] = np.load(periodic['resp_path'])
aperiodic['resp'] = np.load(aperiodic['resp_path'])

# Select data channel
periodic['stim'] = periodic['stim'][:, periodic['stim_channel']]
periodic['resp'] = periodic['resp'][:, periodic['resp_channel']]
aperiodic['resp'] = aperiodic['resp'][:, aperiodic['resp_channel']]

# Normalize signal for height thresholding
periodic['stim_norm'] = periodic['stim']/periodic['stim'].max()
periodic['resp_norm'] = periodic['resp']/periodic['resp'].max()
aperiodic['stim_norm'] = aperiodic['stim']/aperiodic['stim'].max()
aperiodic['resp_norm'] = aperiodic['resp']/aperiodic['resp'].max()


# Find peak indexes in signal
periodic['stim_peaks'] = find_peaks(periodic['stim_norm'],
                                    height=periodic['height_thresh'],
                                    distance=periodic['dist_factor']*periodic['fs'])[0]
periodic['resp_peaks'] = find_peaks(periodic['resp_norm'],
                                    height=periodic['height_thresh'],
                                    distance=periodic['dist_factor']*periodic['fs'])[0]
aperiodic['stim_peaks'] = find_peaks(aperiodic['stim_norm'],
                                     height=aperiodic['height_thresh'],
                                     distance=aperiodic['dist_factor']*aperiodic['fs'])[0]
aperiodic['resp_peaks'] = find_peaks(aperiodic['resp_norm'],
                                     height=aperiodic['height_thresh'],
                                     distance=aperiodic['dist_factor']*aperiodic['fs'])[0]

reaction_str = '{} Signal Average Reaction Time: {:.2f}s +/- {:.2f}s'
# Calculate periodic reaction times
periodic['lim_stim_idx'] = (periodic['stim_peaks'] > periodic['xlim'][0]) & (periodic['stim_peaks'] < periodic['xlim'][1])
periodic['lim_stim_peaks'] = periodic['stim_peaks'][periodic['lim_stim_idx']]
periodic['lim_resp_idx'] = (periodic['resp_peaks'] > periodic['xlim'][0]) & (periodic['resp_peaks'] < periodic['xlim'][1])
periodic['lim_resp_peaks'] = periodic['resp_peaks'][periodic['lim_resp_idx']]
periodic['reaction'] = periodic['resp_peaks'][periodic['resp_peaks'] > periodic['xlim'][0]]
periodic['reaction'] -= periodic['stim_peaks'][periodic['stim_peaks'] > periodic['xlim'][0]]
periodic['reaction'] = periodic['reaction']/periodic['rate']

print(reaction_str.format('Periodic', periodic['reaction'].mean(), periodic['reaction'].std()))

# Calculate aperiodic reaction times
aperiodic['lim_stim_idx'] = (aperiodic['stim_peaks'] > aperiodic['xlim'][0]) & (aperiodic['stim_peaks'] < aperiodic['xlim'][1])
aperiodic['lim_stim_peaks'] = aperiodic['stim_peaks'][aperiodic['lim_stim_idx']]
aperiodic['lim_resp_idx'] = (aperiodic['resp_peaks'] > aperiodic['xlim'][0]) & (aperiodic['resp_peaks'] < aperiodic['xlim'][1])
aperiodic['lim_resp_peaks'] = aperiodic['resp_peaks'][aperiodic['lim_resp_idx']]
aperiodic['reaction'] = aperiodic['resp_peaks'][aperiodic['resp_peaks'] > aperiodic['xlim'][0]]
aperiodic['reaction'] -= aperiodic['stim_peaks'][aperiodic['stim_peaks'] > aperiodic['xlim'][0]]
aperiodic['reaction'] = aperiodic['reaction']/aperiodic['rate']

print(reaction_str.format('Aperiodic', aperiodic['reaction'].mean(), aperiodic['reaction'].std()))

# Plot signals and peaks
fig, ax = plt.subplots(2, 2, figsize=(12, 24))

# Plot Periodic Stimulus + Peaks
ax[0][0].plot(periodic['stim'])
ax[0][0].plot(periodic['stim_peaks'], periodic['stim'][periodic['stim_peaks']], 'o')
# Plot Periodic Response + Peaks
ax[1][0].plot(periodic['resp'])
ax[1][0].plot(periodic['resp_peaks'], periodic['resp'][periodic['resp_peaks']], 'o')
# Set plot options
ax[0][0].set_xlim(periodic['xlim'])
ax[1][0].set_xlim(periodic['xlim'])
ax[0][0].set_title('Input Periodic Signal')
ax[1][0].set_title('Subject Response to Periodic Signal')

# Plot Aperiodic Stimulus + Peaks
ax[0][1].plot(aperiodic['stim'])
ax[0][1].plot(aperiodic['stim_peaks'], aperiodic['stim'][aperiodic['stim_peaks']], 'o')
# Plot Aperiodic Response + Peaks
ax[1][1].plot(aperiodic['resp'])
ax[1][1].plot(aperiodic['resp_peaks'], aperiodic['resp'][aperiodic['resp_peaks']], 'o')
# Set plot options
ax[0][1].set_xlim(aperiodic['xlim'])
ax[1][1].set_xlim(aperiodic['xlim'])
ax[0][1].set_title('Input Aperiodic Signal')
ax[1][1].set_title('Subject Response to Aperiodic Signal')

fig.show()
