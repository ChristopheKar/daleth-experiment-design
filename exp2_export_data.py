import os
import numpy as np
import bioread
from scipy.io import wavfile
import matplotlib.pyplot as plt

data_dir = 'data/exp2'

sampling_rate = 20000

filenames = [f for f in os.listdir(data_dir) if f.endswith('.acq')]

fig, ax = plt.subplots(1, len(filenames), figsize=(20, 10))

for idx, filename in enumerate(filenames):
    filepath = os.path.join(data_dir, filename)
    biopac_data = bioread.read_file(filepath)
    des_channels = ['Stimulus', 'Force']

    data = {}
    for chan in biopac_data.channels:
        if chan.name in des_channels:
            data[chan.name.lower()] = chan.data
            ax[idx].plot(chan.time_index, chan.data, label='{} ({})'.format(chan.name, chan.units))

    ax[idx].set_title(filename)
    ax[idx].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=1, ncol=1, mode="shrink", borderaxespad=0.)
    fig.show()

    arr = np.squeeze(np.dstack([data['stimulus'], data['force']]))
    wavfile.write(filepath.replace('.acq', '.wav'), sampling_rate, arr)
