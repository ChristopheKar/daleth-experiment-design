import os
import numpy as np
from scipy.io import wavfile
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import bioread
import tgre
from praat import dict_to_praat

# define data paths
data_dir = 'data/exp2'
grid_path = os.path.join(data_dir, 'periodic_1_ch1.TextGrid')
data_path = os.path.join(data_dir, 'periodic_1.acq')

# read Praat labeled text grid
grid = tgre.TextGrid.from_file(grid_path)
intervals = grid.tiers[0].to_dict()['intervals']

sampling_rate = 20000
period = 10
safety = 0.8

# read biopac Stimulus and Force signals
biopac_data = bioread.read_file(data_path)
desired_channels = {'stimulus': 0, 'force': 1}
data = {}
data['time'] = biopac_data.channels[0].time_index
for chname, chidx in desired_channels.items():
    # append original signal
    data[chname] = biopac_data.channels[chidx].data
    # append normalized signal
    data[chname + '_norm'] = data[chname]/data[chname].max()

beats, taps = [], []
fig, ax = plt.subplots(2, 7, figsize=(12, 24))
for interval in intervals:
    xmin, xmax = interval['xmin'], interval['xmax']
    cycle_name = interval['text']
    if ('cycle' in cycle_name):
        # get cycle window data
        cycle_num = int(cycle_name.split('cycle')[-1])
        cycle_idx = (data['time'] > float(xmin)) & (data['time'] < float(xmax))
        cycle_time = data['time'][cycle_idx]
        cycle_stim = data['stimulus'][cycle_idx]
        cycle_force = data['force'][cycle_idx]
        # find signal peaks using normalized signals
        stim_peaks_idx = find_peaks(data['stimulus_norm'][cycle_idx], height=0.05, distance=0.4*sampling_rate)[0]
        force_peaks_idx = find_peaks(data['force_norm'][cycle_idx], height=0.05, distance=0.4*sampling_rate)[0]
        # append peaks to beats and taps
        for i, val in enumerate(cycle_time[stim_peaks_idx]):
            grid.tiers[1].insert(val, 'beats{}-{}'.format(cycle_num, i+1))
        for i, val in enumerate(cycle_time[force_peaks_idx]):
            grid.tiers[2].insert(val, 'taps{}-{}'.format(cycle_num, i+1))
        # plot signals and peaks
        ax[0][cycle_num-1].plot(cycle_time, cycle_stim)
        ax[0][cycle_num-1].set_title('Stimulus Signal')
        ax[0][cycle_num-1].plot(cycle_time[stim_peaks_idx], cycle_stim[stim_peaks_idx], 'o')
        ax[1][cycle_num-1].plot(cycle_time, cycle_force)
        ax[1][cycle_num-1].set_title('Force Signal')
        ax[1][cycle_num-1].plot(cycle_time[force_peaks_idx], cycle_force[force_peaks_idx], 'o')

fig.show()
dict_to_praat(grid.to_dict(), grid_path.replace('.TextGrid', '_labels.TextGrid'))
