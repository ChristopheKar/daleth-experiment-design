import time
import numpy as np
import sounddevice as sd
from scipy.io import wavfile

channels = 2
fs = 20000
dtype = np.dtype('int16')

aperiodic_path = 'data/exp1/Aperiodic.wav'
periodic_path = 'data/exp1/PeriodicAlong.wav'

prate, periodic = wavfile.read(periodic_path)
aprate, aperiodic = wavfile.read(aperiodic_path)

print('Get ready for Round 1!')
for s in range(0, 6).__reversed__():
    time.sleep(1)
    print(f'Playback and recording will start in {s} seconds', end='\r')
print('\nGo!')

periodic_rec = sd.playrec(periodic, dtype=dtype, channels=channels)
sd.wait()
np.save('data/exp1/periodic_rec', periodic_rec)

t_break = 10
print()
print(f'Well done! Take a short {t_break} second break!')
time.sleep(t_break)

print('Get ready for Round 2!')
for s in range(0, 6).__reversed__():
    time.sleep(1)
    print(f'Playback and recording will start in {s} seconds', end='\r')
print('\nGo')

aperiodic_rec = sd.playrec(aperiodic, dtype=dtype, channels=channels)
sd.wait()
np.save('data/exp1/aperiodic_rec', aperiodic_rec)

